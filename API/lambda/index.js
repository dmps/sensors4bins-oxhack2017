'use strict';

const AWS = require('aws-sdk');
const doc = require('dynamodb-doc');
var awsClient = new AWS.DynamoDB();
const dynamo = new doc.DynamoDB(awsClient);
const compositionData = require('compositionData.json')

String.prototype.toCamelCase = function () {
    return this.valueOf().replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
        if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
        return index == 0 ? match.toLowerCase() : match.toUpperCase();
    });
}

exports.handler = (event, context, callback) => {
    const done = (err, res) => callback(null, { //returns compostion data as a response
        statusCode: err ? '400' : '200',
        body: err ? err.message : JSON.stringify(res),
        headers: {
            'Content-Type': 'application/json',
        },
    });
    console.log(event)
    var NewItem = { //sets up the new record for the bin
        BinId: event.body.BinId
    }
    for (var item in event.body.items) {
        console.log(item)
        var num = event.body.items[item] //number of copies of item
        for (var material in compositionData[item]) { //adds the composition data (i.e. 50g of Glass) to the payload
            console.log(material,compositionData)
            if (NewItem[material]&& NewItem[material]>0) {
                NewItem[material] += compositionData[item][material] * num
            } else {
                NewItem[material] = compositionData[item][material] * num
            }
        }
    }
    //Deletes the previous record then adds in a new one under the same id
    dynamo.deleteItem({
        TableName: 'Sensors4Bins',
        Key: {
            BinId: event.body.BinId
        }
    }, function (err, data) {
        if (err) console.error(err)
        dynamo.putItem({
            TableName: 'Sensors4Bins',
            Item: NewItem
        }, (err, data) => {
            if (err) {
                done(err, data)
            } else {
                done(null, NewItem)
            }
        })
    })
};